import { useState } from "react";
import uuid from "uuid";

const useTodoState = initialTodos => {

console.log("dddd");
  const [todos, setTodos] = useState(initialTodos);

  return {
    todos,
    addNewTodo: todo => {
      setTodos([...todos, { id: uuid(), task: todo, completed: false }]);
    },
    deleteTodo: id => {
      const newTodos = todos.filter(todo => todo.id !== id);
      //console.log(newTodos);
      setTodos(newTodos);
    },
    toggleTodo: id => {
      const newTodos = todos.map(todo =>
        todo.id === id ? { ...todo, completed: !todo.completed } : { ...todo }
      );
      setTodos(newTodos);
    },
    editTodo: (id, newTask, completedState) => {
      const newTodos = todos.map(todo =>
        todo.id === id ? { ...todo, task: newTask } : { ...todo }
      );
      setTodos(newTodos);
    }
  };
}


export default useTodoState;