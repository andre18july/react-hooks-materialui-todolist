import React from "react";
import useInputState from "./hooks/useInputState";
import { TextField } from "@material-ui/core";

function EditTodo(props) {
  const [todo, setTodo, reset] = useInputState(props.task);


  const submitEditForm = e => {
    e.preventDefault();
    props.editTodo(props.id, todo);
    reset();
    props.toggleEditForm();
  }

  return (
    <form
      onSubmit={submitEditForm}
      style={{marginLeft: '1rem', width: '100%'}}
    >
      <TextField value={todo} onChange={setTodo} margin="normal" fullWidth autoFocus/>
    </form>
  );
}

export default EditTodo;
