import React from "react";
import useInputState from "./hooks/useInputState";
import { Paper, TextField } from "@material-ui/core";

function TodoForm(props) {
  const [task, setTask, resetTask] = useInputState("");

  return (
    <Paper style={{margin: "1rem 0", padding: "0 1rem"}}>
      <form
        onSubmit={e => {
          e.preventDefault();
          props.addNewTodo(task);
          resetTask();
        }}
        
      >
        <TextField
          value={task}
          onChange={setTask}
          margin="normal"
          label="Task Description"
          fullWidth
        />
      </form>
    </Paper>
  );
}

export default TodoForm;
