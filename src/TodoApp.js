import React, { useEffect } from "react";
import TodoList from "./TodoList";
import TodoForm from "./TodoForm";
import useTodoState from "./hooks/useTodoState";

import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";

function TodoApp() {
  const initialTodos = JSON.parse(window.localStorage.getItem("todos") || []);

  const {todos, addNewTodo, deleteTodo, toggleTodo, editTodo} = useTodoState(initialTodos);
  console.log(todos);


  useEffect(() => {
    window.localStorage.setItem("todos", JSON.stringify(todos));
  });


  return (
    <Paper
      style={{
        padding: 0,
        margin: 0,
        height: "100vh",
        backgroundColor: "#fafafa"
      }}
    >
      <AppBar color="primary" position="static" style={{ height: "64px" }}>
        <Toolbar>
          <Typography color="inherit">REACT HOOKS - TODO LIST</Typography>
        </Toolbar>
      </AppBar>
      <Grid container justify="center" style={{ marginTop: "1rem" }}>
        <Grid item xs={11} md={8} lg={4}>
          <TodoForm addNewTodo={addNewTodo} />
          {todos.length > 0 ? (
            <TodoList
              todos={todos}
              deleteTodo={deleteTodo}
              toggleTodo={toggleTodo}
              editTodo={editTodo}
            />
          ) : null}
        </Grid>
      </Grid>
    </Paper>
  );
}

export default TodoApp;
