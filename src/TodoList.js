import React from "react";
import TodoItem from "./TodoItem";

import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";

function TodoList(props) {
  const todos = props.todos.map((todo, index) => (
    <>
      <TodoItem
        {...todo}
        deleteTodo={props.deleteTodo}
        toggleTodo={props.toggleTodo}
        editTodo={props.editTodo}
      />
      {index !== props.todos.length - 1 ? <Divider /> : null }

    </>
  ));

  return (
    <Paper>
      <List>{todos}</List>
    </Paper>
  );
}

export default TodoList;
